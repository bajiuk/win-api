#include "GameObject.h"
#include <cmath>


GameObject::GameObject( double _x, double _y, double _radius, double _speed )
{
	x = _x;
	y = _y;
	radius = _radius;
	speed = _speed;
	vx = 0;
	vy = 0;
}


GameObject::~GameObject()
{
}

void GameObject::setSpeedVector( double _vx, double _vy ) {
	vx = _vx;
	vy = _vy;
}

void GameObject::setSpeedToPoint( double _x, double _y ) {
	vx = _x - x;
	vy = _y - y;
}

void GameObject::move( double dt ) {
	double r = sqrt( vx * vx + vy * vy );
	if ( r > 0.01 ) {
		x += vx / r * speed * dt;
		y += vy / r * speed * dt;
	}
}

bool GameObject::canHitObject( double playerX, double playerY, double playerRadius ) {
	double r = sqrt( ( x - playerX ) * ( x - playerX ) + ( y - playerY ) * ( y - playerY ) );
	return ( playerRadius + radius >= r );
}

void GameObject::draw( HDC dc ) {
}

double GameObject::getX( ) {
	return x;
}

double GameObject::getY( ) {
	return y;
}

double GameObject::getRadius( ) {
	return radius;
}
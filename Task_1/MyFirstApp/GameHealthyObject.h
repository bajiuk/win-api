#pragma once
#include "GameObject.h"
class GameHealthyObject : public GameObject
{
private:
	double health;

public:
	GameHealthyObject( double _x, double _y, double _radius, double _speed, double _health );
	~GameHealthyObject();

	void recalcSpeedVector(double playerX, double playerY);
	void reduceHealth( double damage );
	bool isDead();
	double getHealth();
};


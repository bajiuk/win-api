#pragma once
#include <vector>
#include <fstream>
#include <string>

class GameRating
{
private:
	std::vector<std::pair<int, std::wstring>> rating;
public:
	std::wstring get();
	void insert( int score, std::wstring name );
	void save();
	GameRating();
	~GameRating();
};


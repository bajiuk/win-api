#include "GameEnemy.h"


GameEnemy::GameEnemy( double _x, double _y, double _radius, double _speed, double _health, double _dps ) :
	GameHealthyObject( _x, _y, _radius, _speed, _health )
{
	dps = _dps;
}


GameEnemy::~GameEnemy()
{
}

void GameEnemy::draw( HDC dc ) {
	HBRUSH brush = ::CreateSolidBrush( 0x000000FF );
	HPEN pen = ::CreatePen( PS_SOLID, 1, 0x000000FF );
	HGDIOBJ oldBrush = ::SelectObject( dc, brush );
	HGDIOBJ oldPen = ::SelectObject( dc, pen );
	Ellipse( dc, x - radius, y - radius, x + radius, y + radius );
	::SelectObject( dc, oldBrush );
	::SelectObject( dc, oldPen );
	::DeleteObject( pen );
	::DeleteObject( brush );
}

double GameEnemy::attack() {
	return dps;
}

#pragma once
#include "GameObject.h"
class GameRocket :
	public GameObject
{
protected:
	double damage;
public:
	GameRocket( double _x, double _y, double _radius, double _speed, double _damage );
	~GameRocket();

	void draw( HDC dc );
	double getDamage();
};


#pragma once
#include "GamePlayer.h"
#include "GameEnemy.h"
#include "GameRocket.h"
#include <vector>
#include "GameRating.h"
class Game
{
public:
	
	GamePlayer* player;
	std::vector<GameEnemy> enemies;
	std::vector<GameRocket> rockets;

	RECT rect;
	bool paused;
	GameRating rating;

	void draw( HDC dc );
	void move( double dt );
	void checkHealth( double dt );
	void clearSpeed();

	void gamePause();
	void gameStart();
	bool isPaused();

	void makeEnemy();
	void setClientRect( RECT _rect );

	bool onKeyDown(int code);

	void drawScore( HDC dc );
	void drawHealth( HDC dc );
	void drawHints( HDC dc );

	void gameClear();
	void restart();

	Game();
	~Game();
};


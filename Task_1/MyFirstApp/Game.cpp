#include "Game.h"
#include "GameEnemy.h"


Game::Game()
{
	player = new GamePlayer( 100, 100, 10, 200, 1000, 100 );
}

void Game::draw( HDC dc ) {
	player->draw( dc );
	for ( int i = 0; i < rockets.size( ); ++i ) {
		rockets[ i ].draw( dc );
	}
	for ( int i = 0; i < enemies.size( ); ++i ) {
		enemies[ i ].draw( dc );
	}
	drawScore( dc );
	drawHealth( dc );
	drawHints( dc );
}


Game::~Game()
{
	delete player;
}

void Game::move( double dt ) {
	if ( paused ) {
		return;
	}

	for ( int i = 0; i < enemies.size( ); ++i ) {
		enemies[ i ].setSpeedToPoint( player->getX( ), player->getY( ) );
		enemies[ i ].move( dt );
	}
	player->move( dt );
	for ( int i = 0; i < rockets.size( ); ++i ) {
		rockets[ i ].move( dt );
		double x = rockets[ i ].getX( );
		double y = rockets[ i ].getY( );
		if ( rect.top > y || rect.bottom < y || rect.left > x || rect.right < x ) {
			rockets[ i ] = rockets[ rockets.size() - 1 ];
			rockets.pop_back();
			i -= 1;
		}
	}
	checkHealth( dt );
}

void Game::checkHealth( double dt ) {
	for ( int i = 0; i < enemies.size(); ++i ) {
		for ( int j = 0; j < rockets.size(); ++j ) {
			if ( enemies[ i ].canHitObject( rockets[ j ].getX(), rockets[ j ].getY(), rockets[ j ].getRadius() ) ) {
				enemies[ i ].reduceHealth( rockets[ j ].getDamage() );
				rockets[ j ] = rockets[ rockets.size() - 1 ];
				rockets.pop_back();
				j -= 1;
				if ( enemies[ i ].isDead() ) {
					enemies[ i ] = enemies[ enemies.size() - 1 ];
					enemies.pop_back();
					player->killed += 1;
					i -= 1;
					break;
				} else {
					continue;
				}
			}
		}
	}

	for ( int i = 0; i < enemies.size(); ++i ) {
		if ( player->canHitObject( enemies[ i ].getX(), enemies[ i ].getY(), enemies[ i ].getRadius() ) ) {
			player->reduceHealth( enemies[ i ].attack() );
			enemies[ i ] = enemies[ enemies.size( ) - 1 ];
			enemies.pop_back( );
			i -= 1;
		}
	}

}

bool Game::onKeyDown( int code ) {
	if ( paused ) {
		return true;
	}
	switch ( code ) {
	case 0x41 : // A
		player->left();
		return true;
	case 0x57 : // W
		player->up();
		return true;
	case 0x53 : // S
		player->down();
		return true;
	case 0x44 : // D
		player->right();
		return true;
	case 0x20 : // SPACE
		rockets.push_back(player->fire());
		return true;
	}
	return false;
}

void Game::clearSpeed() {
	player->setSpeedVector( 0, 0 );
}

void Game::setClientRect( RECT _rect ) {
	rect = _rect;
}

void Game::makeEnemy() {
	if ( paused ) {
		return;
	}
	int x = rand() % rect.right;
	int y = rand() % rect.bottom;
	int r = sqrt( ( x - player->getX() ) * ( x - player->getX() ) + ( y - player->getY() ) * ( y - player->getY() ) );
	if ( r < 400 ) {
		x = ( x - player->getX( ) ) / r * 400;
		y = ( y - player->getY( ) ) / r * 400; 
	}

	int speed = rand() % 100 + 30;
	GameEnemy enemy = GameEnemy( x, y, 5, speed, 200, 200 );
	enemies.push_back(enemy);
}

void Game::drawScore( HDC dc ) {
	WCHAR str[ 100 ];
	wsprintf(str, TEXT("Kills %i"), player->killed);
	::SetTextColor( dc, 0x0000FF00 );
	::SetBkMode( dc, TRANSPARENT );

	RECT rect;
	rect.top = 0;
	rect.left = 0;
	rect.bottom = 20;
	rect.right = 300;

	::DrawText( dc, str, lstrlen( str ), &rect, DT_LEFT );	
}

void Game::drawHealth( HDC dc ) {
	WCHAR str[ 100 ];
	wsprintf( str, TEXT( "Health %i" ), (int) player->getHealth() );
	::SetTextColor( dc, 0x0000FF00 );
	::SetBkMode( dc, TRANSPARENT );

	::DrawText( dc, str, lstrlen( str ), &rect, DT_RIGHT );
}

void Game::drawHints( HDC dc ) {
	WCHAR str[ 100 ];
	wsprintf( str, L"Ctrl+P - pause game\n Ctrl+N - new game" );
	::SetTextColor( dc, 0x0000FF00 );
	::SetBkMode( dc, TRANSPARENT );
	rect.top = rect.bottom - 40;
	::DrawText( dc, str, lstrlen( str ), &rect, DT_CENTER );
	rect.top = 0;
}


void Game::gameClear() {
	delete player;
	player = new GamePlayer( rect.right / 2, rect.bottom / 2, 10, 200, 1000, 100 );
	rockets.clear();
	enemies.clear();
}

void Game::restart() {
	gameClear();
}

bool Game::isPaused() {
	return paused;
}
void Game::gamePause() {
	paused = true;
}

void Game::gameStart() {
	paused = false;
}
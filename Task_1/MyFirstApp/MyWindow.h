#pragma once
#include "Game.h"
#include <Windows.h>

class MyWindow
{
public:
	MyWindow();
	virtual ~MyWindow();

	bool Create(HINSTANCE hInstance);
	void CreateDialogWindow( int cmdShow );
	void Show(int cmdShow);
	static bool registerClass(HINSTANCE hInstance);
	HWND dialogHandle;

protected:
	wchar_t* windowTitleName;
	void onDestroy();
	void onPaint();
	void onTimer();
	
	void init();
private:
	double oldTime = 0;
	double lastGenerated = 0;

	Game* game;
	
	UINT timerId;

	static const wchar_t* className;
	HWND handle;
	static LRESULT __stdcall windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lPARAM);
	static BOOL __stdcall dialogProc( HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam );
};


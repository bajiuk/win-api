#include "GameRating.h"
#include "algorithm"
#include <codecvt>
#include <locale>


GameRating::GameRating()
{
	std::wifstream file( "rating.txt" );
	std::wstring temp;
	while ( std::getline( file, temp ) ) {
		int t = temp.find( L" " );
		if ( t == temp.npos ) {
			break;
		}
		std::wstring number = temp.substr( 0, t );
		std::wstring name = temp.substr( t + 1, temp.size() - t );
		int n = std::stoi( number.c_str(), NULL , 10 );
		rating.push_back(std::pair<int, std::wstring> (n, name) ); 
	}
	file.close();
}

std::wstring GameRating::get() {
	std::sort( rating.begin(), rating.end() );
	std::wstring result = L"";
	wchar_t tmp[100];
	for ( int i = rating.size( ) - 1; i >= 0; --i ) {
		result += std::to_wstring( rating[i].first);
		result += L" " + rating[ i ].second + L"\n";
	}
	return result;
}

void GameRating::save() {
	std::sort( rating.begin( ), rating.end( ) );
	std::wofstream out( "rating.txt" );
	const std::locale utf8_locale = std::locale( std::locale( ), new std::codecvt_utf8<wchar_t>( ) );
	out.imbue( utf8_locale );
	for ( int i = rating.size( ) - 1; i >= 0; --i ) {
		out << rating[ i ].first << L" " << rating[ i ].second << std::endl;
	}
	out.close( );
}

void GameRating::insert(int score, std::wstring name) {
	rating.push_back( std::pair<int, std::wstring>( score, name ) );
}


GameRating::~GameRating()
{
}

#include "MyWindow.h"
#include "resource.h"
#include "GameRating.h"
#include "string"

const wchar_t* MyWindow::className = L"Window";

MyWindow::MyWindow()
{
	windowTitleName = L"WINDOW";
	game = new Game();
}

MyWindow::~MyWindow()
{
	free(game);
}

bool MyWindow::Create(HINSTANCE hInstance) 
{
	handle = ::CreateWindowEx(
		WS_EX_CLIENTEDGE,
		className,
		windowTitleName,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 400, 400,
		NULL, NULL, hInstance, NULL);

	if (handle == NULL) {
		::MessageBox(NULL, L"Window Creation Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return false;
	}

	SetLastError(0);
	::SetWindowLong(handle, GWL_USERDATA, (long) this);
	if (GetLastError() != 0) 
	{
		::MessageBox(NULL, L"SetWindowLong failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return false;
	}

	init();
	return true;
}

void MyWindow::CreateDialogWindow( int cmdShow ) {
	dialogHandle = ::CreateDialog( ::GetModuleHandle( 0 ), MAKEINTRESOURCE( IDD_DIALOG1 ), handle, dialogProc );
	::ShowWindow( dialogHandle, cmdShow );
}


void MyWindow::init()
{
	timerId = ::SetTimer(handle, NULL, 1, NULL);
	if (timerId == 0) 
	{
		::MessageBox(NULL, L"SetTimer failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return;
	}

}

void MyWindow::Show(int cmdShow)
{
	::ShowWindow(handle, cmdShow);
	::UpdateWindow(handle);
}

void MyWindow::onDestroy()
{
	::PostQuitMessage(0);
}

void MyWindow::onPaint()
{
	PAINTSTRUCT ps;
	HDC dc = ::BeginPaint(handle, &ps);
	HDC memDc = ::CreateCompatibleDC(dc);
	RECT rect;
	GetWindowRect(handle, &rect);
	HBITMAP memBmp = ::CreateCompatibleBitmap(dc, rect.right, rect.bottom);
	HGDIOBJ oldBmp = ::SelectObject(memDc, memBmp);

	GetClientRect( handle, &rect );
	game->setClientRect( rect );
	
	POINT p;
	::GetCursorPos( &p );
	::ScreenToClient( handle, &p );
	
	game->player->setCursor( p.x, p.y );
	game->draw( memDc );

	::BitBlt(dc, 0, 0, rect.right, rect.bottom, memDc, 0, 0, SRCCOPY);
	
	::SelectObject(memDc, oldBmp);
	::DeleteObject(memBmp);
	::DeleteDC(memDc);
	
	::EndPaint(handle, &ps);
}

double getTime( ) {
	SYSTEMTIME time;
	::GetSystemTime( &time );
	return time.wSecond + 0.001 * time.wMilliseconds + time.wMinute * 60 + time.wHour * 3600;
}



void MyWindow::onTimer() 
{	
	double newTime = getTime();

	if ( ::GetAsyncKeyState( 0x41 ) ) {
		game->onKeyDown( 0x41 );
	}

	if ( ::GetAsyncKeyState( 0x57 ) ) {
		game->onKeyDown( 0x57 );
	}

	if ( ::GetAsyncKeyState( 0x44 ) ) {
		game->onKeyDown( 0x44 );
	}

	if ( ::GetAsyncKeyState( 0x53 ) ) {
		game->onKeyDown( 0x53 );
	}
	if ( oldTime != 0 ) {
		game->move( newTime - oldTime );
	}
	oldTime = newTime;
	
	if ( newTime - lastGenerated > 1 ) {
		lastGenerated = newTime;
		game->makeEnemy();
	}

	if ( game->player->isDead() && !game->isPaused())
	{  
		game->gamePause();
		CreateDialogWindow( SW_SHOW );
	}

	game->clearSpeed();
	RECT rect;
	GetClientRect(handle, &rect);
	InvalidateRect(handle, &rect, false);
}

LRESULT __stdcall MyWindow::windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	long winClassLong = ::GetWindowLong(handle, GWL_USERDATA);
	MyWindow* me = (MyWindow*)winClassLong;

	switch (message)
	{
	case WM_LBUTTONDOWN:
		me->game->onKeyDown( 0x20 );
		break;
	case WM_KEYDOWN:
		if ( me->game->onKeyDown( (int) wParam ) ) {
			break;
		} else {
			return ::DefWindowProc( handle, message, wParam, lParam );
		}
	case WM_COMMAND:
		switch ( LOWORD( wParam ) ) {
		case ID_ACC_NEWGAME:
			me->game->restart();
			break;
		case ID_ACC_PAUSE1:
		case ID_ACC_PAUSE2:
			if ( me->game->isPaused() ) {
				me->game->gameStart();
			}
			else {
				me->game->gamePause( );
			}
			break;
		}
		break;
	case WM_DESTROY:
		me->onDestroy();
		break;
	case WM_PAINT:
		me->onPaint();
		break;
	case WM_TIMER:
		me->onTimer();
		break;
	default:
		return ::DefWindowProc(handle, message, wParam, lParam);
	}
	return 0;
}

bool MyWindow::registerClass(HINSTANCE hInstance)
{
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = windowProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = className;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!::RegisterClassEx(&wc))
	{
		::MessageBox(NULL, L"Window Registration Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return false;
	}

	return true;
}

int StringToWString( std::wstring &ws, const std::wstring &s )
{
	std::wstring wsTmp( s.begin( ), s.end( ) );

	ws = wsTmp;

	return 0;
}

BOOL __stdcall MyWindow::dialogProc( HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam ) {
	MyWindow* me = reinterpret_cast< MyWindow* >( GetWindowLong( GetParent( hwndDlg ), GWL_USERDATA ) );
	switch ( message ) {
	case WM_INITDIALOG:
		{
			std::wstring srating = me->game->rating.get( );
			std::wstring wrating;
			StringToWString( wrating, srating );
			::SetWindowText( ::GetDlgItem( hwndDlg, IDC_SCOREBOARD ), wrating.c_str() );
		    ::SetWindowText( ::GetDlgItem( hwndDlg, IDC_NAME_EDIT ), L"NONAME" );
			return true;
		}
	case WM_COMMAND:
	{
		switch ( LOWORD( wParam ) ) {
		case IDOK:
			wchar_t tmp[ 20 ];
			::GetWindowText( ::GetDlgItem( hwndDlg, IDC_NAME_EDIT ), tmp, 20 );
			me->game->rating.insert( me->game->player->killed, std::wstring(tmp));
			::DestroyWindow( hwndDlg );
			me->game->restart();
			me->game->gameStart();
			me->game->rating.save();
			return TRUE;
		case IDCANCEL:
			::DestroyWindow( hwndDlg );
			me->game->restart();
			me->game->gameStart( );
			return TRUE;
		}
	}
	default:
		return false;
	}
}




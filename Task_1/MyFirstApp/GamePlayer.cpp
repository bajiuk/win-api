#include "GamePlayer.h"
#include <cmath>


GamePlayer::GamePlayer( double _x, double _y, double _radius, double _speed, double _health, double _rocketDamage ) :
	GameHealthyObject(  _x, _y, _radius, _speed, _health )
{
	rocketDamage = _rocketDamage;
}


GamePlayer::~GamePlayer()
{
}


void GamePlayer::draw( HDC dc ) {
	HBRUSH brush = ::CreateSolidBrush( 0x00FF0000 );
	HPEN pen = ::CreatePen( PS_SOLID, 1, 0x0000FF00 );
	HGDIOBJ oldBrush = ::SelectObject( dc, brush );
	HGDIOBJ oldPen = ::SelectObject( dc, pen );
	Ellipse( dc, x - radius, y - radius, x + radius, y + radius );
	
	int r = sqrt( ( x - cursorX ) * ( x - cursorX ) + ( y - cursorY ) * ( y - cursorY ) );
	int weaponX = x + ( cursorX - x ) / r * 20;
	int weaponY = y + ( cursorY - y ) / r * 20;

	MoveToEx( dc, x, y, NULL );
	LineTo( dc, weaponX, weaponY );

	::SelectObject( dc, oldBrush );
	::SelectObject( dc, oldPen );
	::DeleteObject( pen );
	::DeleteObject( brush );
}

void GamePlayer::up( ) {
	vy = -10;
}

void GamePlayer::down( ) {
	vy = +10;
}

void GamePlayer::left( ) {
	vx = -10;
}

void GamePlayer::right( ) {
	vx = +10;
}

GameRocket GamePlayer::fire( ) {
	
	int r = sqrt( ( x - cursorX ) * ( x - cursorX ) + ( y - cursorY ) * ( y - cursorY ) );
	int weaponX = x + ( cursorX - x ) / r * 20;
	int weaponY = y + ( cursorY - y ) / r * 20;

	GameRocket result = GameRocket( weaponX, weaponY, 3, 300, rocketDamage );
	result.setSpeedVector( cursorX - x, cursorY - y );

	return result;
}

void GamePlayer::setCursor( int x, int y ) {
	cursorX = x;
	cursorY = y;
}
#include <windows.h>
#include "MyWindow.h"
#include "resource.h"

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPWSTR lpCmdLine, int nCmdShow)
{
	if (!MyWindow::registerClass(hInstance))
	{
		return 1;
	}
	
	MyWindow wnd;
	
	if (!wnd.Create(hInstance))
	{
		return 1;
	}

	wnd.Show(nCmdShow);

	HACCEL hAccelerators = ::LoadAcceleratorsW( hInstance, MAKEINTRESOURCEW( IDR_ACCELERATOR1 ) );
	MSG msg;
	while ( ::GetMessage( &msg, NULL, 0, 0 ) > 0 ) {
		if ( !::TranslateAcceleratorW( msg.hwnd, hAccelerators, &msg ) && !::IsDialogMessage( wnd.dialogHandle, &msg ) ) {
			::TranslateMessage( &msg );
			::DispatchMessage( &msg );
		}
	}

	::DestroyAcceleratorTable( hAccelerators );
	return msg.wParam;
}
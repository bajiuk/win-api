#pragma once
#include "GameHealthyObject.h"
class GameEnemy :
	public GameHealthyObject
{
protected:
	double dps;
public:
	GameEnemy( double _x, double _y, double _radius, double _speed, double _health, double _dps );
	~GameEnemy();

	double attack();
	void draw( HDC dc);
};


#include "GameHealthyObject.h"
#include <cmath>


GameHealthyObject::GameHealthyObject( double _x, double _y, double _radius, double _speed, double _health ) :
	GameObject( _x, _y, _radius, _speed )
{
	health = _health;
}


GameHealthyObject::~GameHealthyObject()
{
}

void GameHealthyObject::recalcSpeedVector( double playerX, double playerY ) {
	setSpeedVector( playerX - x, playerY - y );
}

bool GameHealthyObject::isDead() {
	return health <= 0;
}

void GameHealthyObject::reduceHealth( double damage ) {
	health -= damage;
}

double GameHealthyObject::getHealth() {
	return health;
}
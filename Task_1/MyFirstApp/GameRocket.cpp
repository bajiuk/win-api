#include "GameRocket.h"


GameRocket::GameRocket( double _x, double _y, double _radius, double _speed, double _damage ) :
	GameObject( _x, _y, _radius, _speed )
{
	damage = _damage;
}


GameRocket::~GameRocket()
{
}

void GameRocket::draw( HDC dc ) {
	HBRUSH brush = ::CreateSolidBrush( 0x00FF0000 );
	HPEN pen = ::CreatePen( PS_SOLID, 1, 0x0000FF00 );
	HGDIOBJ oldBrush = ::SelectObject( dc, brush );
	HGDIOBJ oldPen = ::SelectObject( dc, pen );
	Ellipse( dc, x - radius, y - radius, x + radius, y + radius );

	::SelectObject( dc, oldBrush );
	::SelectObject( dc, oldPen );
	::DeleteObject( pen );
	::DeleteObject( brush );
}

double GameRocket::getDamage() {
	return damage;
}

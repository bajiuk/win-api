#pragma once
#include <Windows.h>
class GameObject
{
protected:
	double x;
	double y;

	double vx;
	double vy;

	double radius;
	double speed;

	int color;

public:
	GameObject( double _x, double _y, double _radius, double _speed );
	~GameObject();

	void setSpeedVector(double _vx, double _vy);
	void setSpeedToPoint( double _x, double _y );
	bool canHitObject( double playerX, double playerY, double playerRadius );
	void move( double dt );

	virtual void draw( HDC dc );


	double getX( );
	double getY( );
	double getRadius();
};


#pragma once
#include "GameHealthyObject.h"
#include "GameRocket.h"
class GamePlayer :
	public GameHealthyObject
{
protected:
	double rocketDamage;

	int cursorX;
	int cursorY;

public:
	GamePlayer( double _x, double _y, double _radius, double _speed, double _health, double rocketDamage );
	~GamePlayer();

	void draw( HDC dc );

	void setCursor( int x, int y );

	void up();
	void down();
	void left();
	void right();

	int killed = 0;

	GameRocket fire();
	void incEnemiesKilled();
};


#pragma once
#include <Windows.h>
class CBall
{
public:
	CBall( int _x, int _y, int _r );
	~CBall();
	void setVelocity( int _x, int _y );
	void move();
	void setRect( RECT rect );
	void Draw( HDC dc );
	void changeColor( boolean isFocused );
private:
	bool isInField( int vx, int vy );
	COLORREF color;
	int radius;
	int x;
	int y;
	int vx;
	int vy;
	RECT field;

};


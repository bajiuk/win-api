#pragma once
#include "Ball.h"

#include <Windows.h>

class CEllipseWindow
{
public:
	CEllipseWindow();
	virtual ~CEllipseWindow();

	HWND Create( HINSTANCE hInstance, HWND parent );
	void Show( int cmdShow );
	static bool __stdcall registerClass( HINSTANCE hInstance );

protected:
	static wchar_t* windowTitleName;
	static wchar_t* windowClassName;
	
	void onDestroy();
	void onPaint();
	void onTimer();
	void onLButtonClick();

	void init();

private:
	CBall* ball;
	UINT timerId;
	void drawEllipse( HDC dc );


	HWND handle;
	static LRESULT __stdcall windowProc( HWND handle, UINT message, WPARAM wParam, LPARAM lPARAM );
};


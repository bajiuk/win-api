#include <windows.h>
#include "MainWindow.h"
#include "EllipseWindow.h"

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow )
{
	if ( !COverlappedWindow::registerClass( hInstance ) )
	{
		return 1;
	}

	if ( !CEllipseWindow::registerClass( hInstance ) ) {
		return 1;
	}
	
	COverlappedWindow wnd;
	
	if ( !wnd.Create( hInstance ) )
	{
		return 1;
	}

	wnd.Show( nCmdShow );

	MSG msg;
	while ( GetMessage( &msg, NULL, 0, 0 ) > 0 )
	{
		TranslateMessage( &msg );
		DispatchMessage( &msg );
	}
	return msg.wParam;
}
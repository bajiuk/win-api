#include "EllipseWindow.h"

wchar_t* CEllipseWindow::windowTitleName = L"Ball Window";
wchar_t* CEllipseWindow::windowClassName = L"Ball Window Class";

CEllipseWindow::CEllipseWindow()
{
	ball = new CBall( 100, 100, 10 );
	ball->setVelocity( 2, 2 );
}


CEllipseWindow::~CEllipseWindow()
{
	free( ball );
}

HWND CEllipseWindow::Create( HINSTANCE hInstance, HWND parent )
{
	handle = ::CreateWindowEx(
		NULL,
		windowClassName,
		windowTitleName,
		WS_CHILDWINDOW | WS_BORDER | WS_CLIPSIBLINGS,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		parent, NULL, ::GetModuleHandle( 0 ), NULL );

	if (handle == NULL) {
		::MessageBox( NULL, L"Window Creation Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK );
		return false;
	}

	SetLastError( 0 );
	::SetWindowLong( handle, GWL_USERDATA, (long) this );
	if (GetLastError() != 0) 
	{
		::MessageBox( NULL, L"SetWindowLong failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK );
		return false;
	}

	init();
	return handle;
}

void CEllipseWindow::init()
{
	timerId = ::SetTimer( handle, NULL, 10, NULL );
	if (timerId == 0) 
	{
		::MessageBox( NULL, L"SetTimer failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK );
		return;
	}
}

void CEllipseWindow::Show( int cmdShow )
{
	::ShowWindow( handle, cmdShow );
	::UpdateWindow( handle );
}

void CEllipseWindow::onDestroy()
{
}

void CEllipseWindow::drawEllipse( HDC dc )
{
	RECT rect;

	// Background painting
	GetClientRect( handle, &rect );
	HBRUSH brush = ::CreateSolidBrush( 0x00ff0000 );
	FillRect( dc, &rect, brush );
	::DeleteObject( brush );

	// Drawing Ellipse
	ball->setRect( rect );
	ball->Draw( dc );
	ball->move();
}

void CEllipseWindow::onPaint()
{
	ball->changeColor( ::GetFocus() == handle );

	PAINTSTRUCT ps;
	HDC dc = ::BeginPaint( handle, &ps );
	HDC memDc = ::CreateCompatibleDC( dc );
	RECT rect;
	GetWindowRect( handle, &rect );
	HBITMAP memBmp = ::CreateCompatibleBitmap( dc, rect.right, rect.bottom );
	HGDIOBJ oldBmp = ::SelectObject( memDc, memBmp );
	
	drawEllipse( memDc );

	::BitBlt( dc, 0, 0, rect.right, rect.bottom, memDc, 0, 0, SRCCOPY );
	
	::SelectObject( memDc, oldBmp );
	::DeleteObject( memBmp );
	::DeleteDC( memDc );
	
	::EndPaint( handle, &ps );
}

void CEllipseWindow::onTimer() 
{
	RECT rect;
	GetClientRect( handle, &rect );
	InvalidateRect( handle, &rect, false );
}

void CEllipseWindow::onLButtonClick() {
	::SetFocus( handle );
}

LRESULT __stdcall CEllipseWindow::windowProc( HWND handle, UINT message, WPARAM wParam, LPARAM lParam )
{
	long winClassLong = ::GetWindowLong( handle, GWL_USERDATA );
	CEllipseWindow* me = (CEllipseWindow*) winClassLong;

	switch ( message )
	{
		case WM_CLOSE:
			DestroyWindow( handle );
			break;
		case WM_DESTROY:
			PostQuitMessage( 0 );
			break;
		case WM_PAINT:
			me->onPaint();
			break;
		case WM_TIMER:
			me->onTimer();
			break;
		case WM_LBUTTONDOWN:
			me->onLButtonClick();
			break;
		default:
			return ::DefWindowProc( handle, message, wParam, lParam );
	}
	return 0;
}

bool __stdcall CEllipseWindow::registerClass( HINSTANCE hInstance )
{
	WNDCLASSEX wc;
	wc.cbSize = sizeof( WNDCLASSEX );
	wc.style = 0;
	wc.lpfnWndProc = windowProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon( NULL, IDI_APPLICATION );
	wc.hCursor = LoadCursor( NULL, IDC_ARROW );
	wc.hbrBackground = (HBRUSH)( COLOR_WINDOW + 1 );
	wc.lpszMenuName = NULL;
	wc.lpszClassName = windowClassName;
	wc.hIconSm = LoadIcon( NULL, IDI_APPLICATION );

	if ( !::RegisterClassEx( &wc ) )
	{
		::MessageBox( NULL, L"Window Registration Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK );
		return false;
	}

	return true;
}



#pragma once
#include "Ball.h"

#include <Windows.h>

class COverlappedWindow
{
public:
	COverlappedWindow();
	virtual ~COverlappedWindow();

	bool Create( HINSTANCE hInstance );
	void Show( int cmdShow );
	static bool __stdcall registerClass( HINSTANCE hInstance );

protected:
	static wchar_t* windowTitleName;
	static wchar_t* windowClassName;
	
	void onDestroy();
	void onSize();

	void resizeChildren();
	void init();
private:

	HWND children[4];
	HINSTANCE hInstance;
	HWND handle;
	static LRESULT __stdcall windowProc( HWND handle, UINT message, WPARAM wParam, LPARAM lPARAM );
};


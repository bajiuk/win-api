#include "MainWindow.h"
#include "EllipseWindow.h"


wchar_t* COverlappedWindow::windowClassName = L"Main Window Class";
wchar_t* COverlappedWindow::windowTitleName = L"Main Window";


COverlappedWindow::COverlappedWindow()
{
}


COverlappedWindow::~COverlappedWindow()
{
}

bool COverlappedWindow::Create( HINSTANCE _hInstance ) 
{
	hInstance = _hInstance;
	handle = ::CreateWindow(
		windowClassName,
		windowTitleName,
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		CW_USEDEFAULT, CW_USEDEFAULT, 400, 400,
		NULL, NULL, hInstance, NULL );

	if (handle == NULL) {
		::MessageBox( NULL, L"Window Creation Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK );
		return false;
	}

	SetLastError(0);
	::SetWindowLong( handle, GWL_USERDATA, (long) this );
	if ( GetLastError() != 0 ) 
	{
		::MessageBox( NULL, L"SetWindowLong failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK );
		return false;
	}

	init();

	return true;
}

void COverlappedWindow::init()
{
	for( int i = 0; i < 4; ++i )
	{
		CEllipseWindow* wnd = new CEllipseWindow();
		children[i] = wnd->Create( ::GetModuleHandle(0), handle );
	}
}

void COverlappedWindow::resizeChildren() {
	RECT rect;
	::GetClientRect( handle, &rect );

	int xSize = rect.right / 2;
	int ySize = rect.bottom / 2;

	for( int i = 0; i < 2; ++i )
	{
		for( int j = 0; j < 2; ++j )
		{
			::SetWindowPos( children[i * 2 + j], NULL, i * xSize, j * ySize, xSize, ySize, SWP_NOZORDER );
		}
	}
}

void COverlappedWindow::Show( int cmdShow )
{
	::ShowWindow( handle, cmdShow );
	::UpdateWindow( handle );
	for( int i = 0; i < 4; ++i )
	{
		::ShowWindow( children[i], SW_SHOW );
		::UpdateWindow( children[i] );
	}
}

void COverlappedWindow::onDestroy()
{
}

void COverlappedWindow::onSize() {
	resizeChildren();
}

LRESULT __stdcall COverlappedWindow::windowProc( HWND handle, UINT message, WPARAM wParam, LPARAM lParam )
{
	COverlappedWindow* me = reinterpret_cast<COverlappedWindow*> ( ::GetWindowLong(handle, GWL_USERDATA) );

	switch (message)
	{
	case WM_CLOSE:
		DestroyWindow( handle );
		break;
	case WM_DESTROY:
		PostQuitMessage( 0 );
		break;
	case WM_SIZE:
		me->onSize();
		break;
	default:
		return ::DefWindowProc( handle, message, wParam, lParam );
	}
	return 0;
}

bool __stdcall COverlappedWindow::registerClass( HINSTANCE hInstance )
{
	WNDCLASSEX wc;
	wc.cbSize = sizeof( WNDCLASSEX );
	wc.style = 0;
	wc.lpfnWndProc = windowProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon( NULL, IDI_APPLICATION );
	wc.hCursor = LoadCursor( NULL, IDC_ARROW );
	wc.hbrBackground = (HBRUSH)( COLOR_WINDOW + 1 );
	wc.lpszMenuName = NULL;
	wc.lpszClassName = windowClassName;
	wc.hIconSm = LoadIcon( NULL, IDI_APPLICATION );

	if ( !::RegisterClassEx( &wc ) )
	{
		::MessageBox( NULL, L"Window Registration Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK );
		return false;
	}

	return true;
}



#include "Ball.h"


CBall::CBall( int _x, int _y, int _r )
{
	x = _x;
	y = _y;
	radius = _r;
	changeColor( false );
}

void CBall::changeColor( boolean isFocused ) {
	if (isFocused)
	{
		color = RGB( 255, 0, 0 );
	} else {
		color = RGB( 0, 255, 0 );
	}
}

void CBall::setVelocity( int _x, int _y )
{
	vx = _x;
	vy = _y;
}

bool CBall::isInField( int vx,  int vy ) {
	return ( x + vx + radius < field.right && x + vx - radius > field.left
		&& y + vy + radius < field.bottom && y + vy - radius > field.top );
}

void CBall::move() 
{
	if ( isInField( vx, vy ) )
	{
		x += vx;
		y += vy;
		return;
	}
	
	if ( isInField( -vx, vy ) )
	{
		vx = -vx;
		x += vx;
		y += vy;
		return;
	}
	
	if ( isInField( vx, -vy ) )
	{
		vy = -vy;
		x += vx;
		y += vy;
		return;
	}

	if ( isInField( -vx, -vy ) )
	{
		vy = -vy;
		vx = -vx;
		x += vx;
		y += vy;
		return;
	}

}

void CBall::setRect( RECT rect ) 
{
	field = rect;
}

void CBall::Draw( HDC dc )
{
	HBRUSH brush = ::CreateSolidBrush( color );
	HPEN pen = ::CreatePen( PS_SOLID, 1, 0x000000FF );
	::SelectObject( dc, brush );
	::SelectObject( dc, pen );
	Ellipse( dc, x - radius, y - radius, x + radius, y + radius );
	::DeleteObject( pen );
	::DeleteObject( brush );
}


CBall::~CBall()
{
}

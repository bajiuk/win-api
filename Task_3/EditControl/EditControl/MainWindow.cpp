#include "MainWindow.h"
#include "resource.h"
#include <CommCtrl.h>
#include <CommDlg.h>
#include <tchar.h>

wchar_t* COverlappedWindow::windowClassName = L"Main Window Class";

COverlappedWindow::COverlappedWindow() :
handle( 0 ),
editHandle( 0 ),
hInstance( 0 )
{
	backgroundBrush = ::CreateSolidBrush( oldSettings.backgroundColor );
}

int getPixelSize( int textSize ) {
	HDC dc = ::GetDC( NULL );
	int size = MulDiv( textSize, ::GetDeviceCaps( dc, LOGPIXELSY ), 72 );
	::DeleteDC( dc );
	return size;
}

COverlappedWindow::~COverlappedWindow()
{
	::DeleteObject( backgroundBrush );
	if ( font != NULL ) {
		::DeleteObject( font );
	}
}

bool COverlappedWindow::Create( HINSTANCE _hInstance )
{
	hInstance = _hInstance;

	wchar_t* windowTitleName = new wchar_t[ 100 ];
	::LoadString( hInstance, IDS_STRING_APP_NAME, windowTitleName, 100 );

	handle = ::CreateWindowEx( WS_EX_LAYERED,
		windowClassName,
		windowTitleName,
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		NULL, NULL, hInstance, NULL );

	if ( handle == NULL ) {
		::MessageBox( NULL, L"Window Creation Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK );
		return false;
	}

	::SetWindowLong( handle, GWL_USERDATA, ( long ) this );
	if ( GetLastError() != 0 ) {
		::MessageBox( NULL, L"SetWindowLong failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK );
		return false;
	}

	init();

	return true;
}

void COverlappedWindow::CreateDialogWindow( int cmdShow ) {
	if ( dialogHandle != NULL ) {
		return;
	}
	dialogHandle = ::CreateDialog( ::GetModuleHandle( 0 ), MAKEINTRESOURCE( IDD_SETTINGS_DIALOG ), handle, dialogProc );

	HWND hTransparency = ::GetDlgItem( dialogHandle, IDC_TRANSPARENCY );
	HWND hTextSize = ::GetDlgItem( dialogHandle, IDC_TEXT_SIZE );

	::SendMessage( hTransparency, TBM_SETRANGE, FALSE, MAKELPARAM( 0, 255 ) );
	::SendMessage( hTransparency, TBM_SETPOS, TRUE, oldSettings.transparency );
	::SendMessage( hTextSize, TBM_SETRANGE, FALSE, MAKELPARAM( 8, 72 ) );
	::SendMessage( hTextSize, TBM_SETPOS, TRUE, oldSettings.textSize );

	newSettings = oldSettings;

	::ShowWindow( dialogHandle, cmdShow );
}

void COverlappedWindow::init()
{
	editHandle = ::CreateWindowEx(
		0, L"EDIT",
		NULL,
		WS_CHILD | WS_VISIBLE | WS_VSCROLL |
		ES_LEFT | ES_MULTILINE,
		0, 0, 0, 0,
		handle,
		NULL,
		hInstance,
		NULL );
	loadTextToEdit();

	LOGFONT logFont;
	::ZeroMemory( &logFont, sizeof( logFont ) );
	logFont.lfHeight = -getPixelSize( oldSettings.textSize );
	_tcscpy_s( logFont.lfFaceName, L"Arial" );

	if ( font != NULL ) {
		::DeleteObject( font );
	}
	font = ::CreateFontIndirect( &logFont );
	::SendMessage( editHandle, WM_SETFONT, WPARAM( font ), TRUE );
	
	
	SetLayeredWindowAttributes( handle, RGB( 0, 0, 0 ), oldSettings.transparency, LWA_ALPHA );
}

void COverlappedWindow::loadTextToEdit()
{
	HMODULE hModule = ::GetModuleHandle( NULL );
	HRSRC hResource = ::FindResource( hModule, MAKEINTRESOURCE( IDR_EDITTEXT_TEXT1 ), L"EDITTEXT_TEXT" );
	HGLOBAL hMemory = ::LoadResource( hModule, hResource );
	DWORD dwSize = SizeofResource( hModule, hResource ) / sizeof( wchar_t );
	LPVOID lpAddress = LockResource( hMemory );
	wchar_t *bytes = new wchar_t[ dwSize + 1 ];
	memcpy( bytes, lpAddress, dwSize * sizeof( wchar_t ) );
	bytes[ dwSize ] = 0;
	::SendMessage( editHandle, WM_SETTEXT, NULL, ( LPARAM ) bytes );
	free( bytes );
}

void COverlappedWindow::resizeChildren() {
	RECT rect;
	::GetClientRect( handle, &rect );
	::SetWindowPos( editHandle, NULL, 0, 0, rect.right, rect.bottom, 0 );
}

void COverlappedWindow::Show( int show )
{
	cmdShow = show;
	::ShowWindow( handle, cmdShow );
	::UpdateWindow( handle );
}

void COverlappedWindow::onDestroy()
{
	PostQuitMessage( 0 );
}

void COverlappedWindow::onSize()
{
	resizeChildren();
}

void COverlappedWindow::onCommand( WPARAM wParam, LPARAM lParam )
{
	switch ( HIWORD( wParam ) ) {
		case EN_CHANGE:
			edited = true;
			return;
	}

	switch ( LOWORD( wParam ) ) {
		case ID_FILE_SAVE:
			saveToFile();
			return;
		case ID_FILE_EXIT:
			onClose();
			return;
		case ID_FILE_SETTINGS:
			CreateDialogWindow( cmdShow );
			return;
	}

	switch ( LOWORD( wParam ) ) {
		case ID_QUIT:
			::DestroyWindow( handle );
		default:
			break;
	}
}

LRESULT COverlappedWindow::onCtlColorEdit(WPARAM wParam) {
	WindowSettings* settings = &oldSettings;
	if ( dialogHandle != NULL ) {
		if ( SendMessage( GetDlgItem( dialogHandle, IDC_PREVIEW ), BM_GETCHECK, 0, 0 ) == BST_CHECKED ) {
			settings = &newSettings;
		}
	}

	::SetTextColor( ( HDC ) wParam, settings->textColor );
	::SetBkColor( ( HDC ) wParam, settings->backgroundColor );
	
	::DeleteObject( backgroundBrush ); 
	backgroundBrush = ::CreateSolidBrush( settings->backgroundColor );
	return ( LRESULT ) backgroundBrush;
}

void COverlappedWindow::onClose() {
	if ( !edited ) {
		::DestroyWindow( handle );
		return;
	}

	int result = ::MessageBox( handle, L"C�������� ��������?", L"", MB_YESNOCANCEL );

	switch ( result ) {
	case IDNO:
		::DestroyWindow( handle );
		return;
	case IDYES:
		if ( saveToFile() ) {
			::DestroyWindow( handle );
		}
		return;
	case IDCANCEL:
		return;
	}
}

boolean COverlappedWindow::saveToFile()
{
	OPENFILENAME ofn;

	char szFileName[ MAX_PATH ] = "";
	ZeroMemory( &ofn, sizeof( ofn ) );
	ofn.lStructSize = sizeof( ofn );
	ofn.hwndOwner = handle;
	ofn.lpstrFilter = ( LPCWSTR ) L"Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
	ofn.lpstrFile = ( LPWSTR ) szFileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = ( LPCWSTR ) L"txt";

	if ( !::GetSaveFileName( &ofn ) ) {
		return false;
	}

	HANDLE f = ::CreateFile(
		ofn.lpstrFile,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL );

	int textLength = ::SendMessage( editHandle, WM_GETTEXTLENGTH, 0, 0 );
	wchar_t* text = new wchar_t[ textLength ];
	::SendMessage( editHandle, WM_GETTEXT, ( textLength ) * sizeof( wchar_t ), ( LPARAM ) text );

	DWORD bytesWritten;
	const unsigned short bom = 0xFEFF;

	::WriteFile( f, &bom, 2, &bytesWritten, NULL );
	::WriteFile( f, text, ( textLength ) * sizeof( wchar_t ), &bytesWritten, NULL );

	::CloseHandle( f );
	edited = false;
	return true;
}

void COverlappedWindow::RefreshSettings( WindowSettings settings ) {
	::SetLayeredWindowAttributes( handle, RGB( 0, 0, 0 ), settings.transparency, LWA_ALPHA );
	LOGFONT logFont;

	font = ( HFONT ) SendMessage( editHandle, WM_GETFONT, 0, 0 );
	::GetObject( font, sizeof( LOGFONT ), &logFont );
	::DeleteObject( font );
	logFont.lfHeight = -getPixelSize( settings.textSize );
	font = ::CreateFontIndirect( &logFont );
	::SendMessage( editHandle, WM_SETFONT, WPARAM( font ), TRUE );
}

LRESULT __stdcall COverlappedWindow::windowProc( HWND handle, UINT message, WPARAM wParam, LPARAM lParam )
{
	COverlappedWindow* me = reinterpret_cast< COverlappedWindow* > ( ::GetWindowLong( handle, GWL_USERDATA ) );

	switch ( message )	{
	case WM_CLOSE:
		me->onClose();
		break;
	case WM_DESTROY:
		me->onDestroy();
		break;
	case WM_SIZE:
		me->onSize();
		break;
	case WM_COMMAND:
		me->onCommand( wParam, lParam );
		return 0;
	case WM_CTLCOLOREDIT:
		return me->onCtlColorEdit(wParam);
	default:
		return ::DefWindowProc( handle, message, wParam, lParam );
	}
	return 0;
}

bool __stdcall COverlappedWindow::registerClass( HINSTANCE hInstance )
{
	WNDCLASSEX wc;
	wc.cbSize = sizeof( WNDCLASSEX );
	wc.style = 0;
	wc.lpfnWndProc = windowProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = ::LoadIcon( hInstance, MAKEINTRESOURCE( IDI_ICON1 ) );
	wc.hCursor = ::LoadCursor( NULL, IDC_ARROW );
	wc.hbrBackground = ( HBRUSH ) ( COLOR_WINDOW + 1 );
	wc.lpszMenuName = MAKEINTRESOURCE( IDR_MENU1 );
	wc.lpszClassName = windowClassName;
	wc.hIconSm = reinterpret_cast<HICON> (::LoadImage( hInstance, MAKEINTRESOURCE( IDI_ICON1 ), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR ));

	if ( !::RegisterClassEx( &wc ) ) {
		::MessageBox( NULL, L"Window Registration Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK );
		return false;
	}

	return true;
}


BOOL __stdcall COverlappedWindow::dialogProc( HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam ) {
	COverlappedWindow* me = reinterpret_cast< COverlappedWindow* >( GetWindowLong( GetParent( hwndDlg ), GWL_USERDATA ) );

	switch ( message ) {
	case WM_HSCROLL:
	{
		HWND hTransparency = GetDlgItem( hwndDlg, IDC_TRANSPARENCY );
		HWND hTextSize = GetDlgItem( hwndDlg, IDC_TEXT_SIZE );

		me->newSettings.transparency = SendMessage( hTransparency, TBM_GETPOS, 0, 0 );
		me->newSettings.textSize = SendMessage( hTextSize, TBM_GETPOS, 0, 0 );

		if ( SendMessage( GetDlgItem( hwndDlg, IDC_PREVIEW ), BM_GETCHECK, 0, 0 ) == BST_CHECKED ) {
			me->RefreshSettings( me->newSettings);
		}
		return FALSE;
	}
	case WM_COMMAND:
	{
		COLORREF customColors[ 16 ];
		for ( int i = 0; i < 16; ++i ) {
			customColors[ i ] = RGB( 255, 255, 255 );
		}

		CHOOSECOLOR color;
		ZeroMemory( &color, sizeof( color ) );
		color.lStructSize = sizeof( color );
		color.lpCustColors = ( LPDWORD ) customColors;
		color.rgbResult = 0;
		color.Flags = CC_FULLOPEN | CC_RGBINIT;
		color.hwndOwner = hwndDlg;

		switch ( LOWORD( wParam ) ) {
		case IDOK:
			me->RefreshSettings( me->newSettings);
			me->oldSettings = me->newSettings;
			DestroyWindow( hwndDlg );
			me->dialogHandle = NULL;
			return TRUE;
		case IDCANCEL:
			me->newSettings = me->oldSettings;
			me->RefreshSettings( me->oldSettings);
			DestroyWindow( hwndDlg );
			me->dialogHandle = NULL;
			return TRUE;
		case IDC_PREVIEW:
		{
			if ( SendMessage( GetDlgItem( hwndDlg, IDC_PREVIEW ), BM_GETCHECK, 0, 0 ) == BST_UNCHECKED ) {
				me->RefreshSettings( me->oldSettings);
			} else {
				me->RefreshSettings( me->newSettings);
			}
			return FALSE;
		}
		case IDC_COLOR_BACKGROUND: 
			if ( ::ChooseColor( &color ) ) {
				me->newSettings.backgroundColor = color.rgbResult;
				if ( SendMessage( GetDlgItem( hwndDlg, IDC_PREVIEW ), BM_GETCHECK, 0, 0 ) == BST_CHECKED ) {
					me->RefreshSettings( me->newSettings);
				}
			}
			return FALSE;
		case IDC_COLOR_FONT:
			if ( ::ChooseColor( &color ) ) {
				me->newSettings.textColor = color.rgbResult;
				if ( SendMessage( GetDlgItem( hwndDlg, IDC_PREVIEW ), BM_GETCHECK, 0, 0 ) == BST_CHECKED ) {
					me->RefreshSettings( me->newSettings);
				}
			}
			return FALSE;
		}
		}
	}
	return FALSE;
}

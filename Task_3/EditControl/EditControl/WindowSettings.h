#pragma once
#include <Windows.h>
class WindowSettings
{
public:
	COLORREF textColor;
	COLORREF backgroundColor;
	int textSize;
	int transparency;
	WindowSettings();
	~WindowSettings();
};


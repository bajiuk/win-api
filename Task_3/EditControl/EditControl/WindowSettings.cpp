#include "WindowSettings.h"


WindowSettings::WindowSettings()
{
	transparency = 255;
	textSize = 12;
	textColor = RGB( 0, 0, 0 );
	backgroundColor = RGB( 255, 255, 255 );
}


WindowSettings::~WindowSettings()
{
}

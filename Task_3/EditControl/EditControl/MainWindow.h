#pragma once
#include <Windows.h>
#include "WindowSettings.h"

class COverlappedWindow {
public:
	COverlappedWindow();
	virtual ~COverlappedWindow();

	bool Create( HINSTANCE hInstance );
	void CreateDialogWindow( int cmdShow );
	void Show( int cmdShow );

	static bool __stdcall registerClass( HINSTANCE hInstance );

	HWND dialogHandle;
private:
	static wchar_t* windowClassName;
	boolean edited = false;
	HWND editHandle;
	HINSTANCE hInstance;
	HWND handle;
	HBRUSH backgroundBrush;
	HFONT font;

	int cmdShow;
	
	WindowSettings oldSettings;
	WindowSettings newSettings;

	void RefreshSettings( WindowSettings settings );
	void onDestroy();
	void onSize();
	void onClose();
	void onCommand(WPARAM wParam, LPARAM lParam);
	LRESULT onCtlColorEdit( WPARAM wParam );

	void resizeChildren();
	void init();
	void loadTextToEdit();
	boolean saveToFile();

	static LRESULT __stdcall windowProc( HWND handle, UINT message, WPARAM wParam, LPARAM lParam );
	static BOOL __stdcall dialogProc( HWND handle, UINT message, WPARAM wParam, LPARAM lParam );
};

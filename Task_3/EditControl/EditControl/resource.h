//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by EditControl.rc
//
#define IDI_ICON1                       101
#define IDR_MENU1                       103
#define IDR_ACCELERATOR1                107
#define IDS_STRING_APP_NAME             109
#define IDR_EDITTEXT_TEXT1              117
#define IDD_SETTINGS_DIALOG             118
#define IDC_COLORS                      1001
#define IDC_COLOR_BACKGROUND            1002
#define IDC_COLOR_FONT                  1003
#define IDC_TEXT_SIZE                   1005
#define IDC_TRANSPARENCY                1006
#define IDC_STATIC_TRANSPARENCY         1007
#define IDC_STATIC_TEXT_SIZE            1008
#define IDC_CHECK1                      1009
#define IDC_PREVIEW                     1009
#define ID_FILE_SAVE                    40002
#define ID_FILE_EXIT                    40003
#define ID_QUIT                         40004
#define ID_FILE_SETTINGS                40007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        119
#define _APS_NEXT_COMMAND_VALUE         40008
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
